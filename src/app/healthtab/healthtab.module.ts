import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HealthtabPageRoutingModule } from './healthtab-routing.module';

import { HealthtabPage } from './healthtab.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HealthtabPageRoutingModule
  ],
  declarations: [HealthtabPage]
})
export class HealthtabPageModule {}
