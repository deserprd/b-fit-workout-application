import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { min } from 'rxjs/operators';
import { ModalPage } from '../modal/modal.page';

@Component({
  selector: 'app-fullbody',
  templateUrl: './fullbody.page.html',
  styleUrls: ['./fullbody.page.scss'],
})
export class FullbodyPage implements OnInit {

  constructor(public modalController: ModalController) {
    
   }

  ngOnInit() {
  }

  async presentModal() {
    
    const modal = await this.modalController.create({
      component: ModalPage,
      componentProps: {
        'exerName': "Push Up",
        'exerCount': "16",
        'exerDetails': "Get down on all fours, placing your hands slightly wider than your shoulders. Straighten your arms and legs.",
        'exerDetails2': "Lower your body until your chest nearly touches the floor. Pause, then push yourself back up. Repeat.",
        'exerImgName': "pushup"
      }
    });
    return await modal.present();
  }

  async presentModal2() {
    
    const modal = await this.modalController.create({
      component: ModalPage,
      componentProps: {
        'exerName': "Squats",
        'exerCount': "16",
        'exerDetails': "Stand with feet a little wider than hip width, toes facing front. Drive your hips back—bending at the knees and ankles and pressing your knees slightly open. Sit into a squat position while still keeping your heels and toes on the ground, chest up and shoulders back.",
        'exerDetails2': "Strive to eventually reach parallel, meaning knees are bent to a 90-degree angle. Press into your heels and straighten legs to return to a standing upright position.",
        'exerImgName': "squats"
      }
    });
    return await modal.present();
  }

  async presentModal3() {
    
    const modal = await this.modalController.create({
      component: ModalPage,
      componentProps: {
        'exerName': "Running",
        'exerCount': "5Mins",
        'exerDetails': "Postition yourself into a great place where there is no vehicles or obstacles. Run your feet alternately.",
        'exerImgName': "running"
      }
    });
    return await modal.present();
  }

  
  async presentModal4() {
    
    const modal = await this.modalController.create({
      component: ModalPage,
      componentProps: {
        'exerName': "Lunges",
        'exerCount': "16",
        'exerDetails': "Stand tall with feet hip-width apart. Engage your core. Take a big step forward with right leg. Start to shift your weight forward so heel hits the floor first. Lower your body until right thigh is parallel to the floor and right shin is vertical. It’s OK if knee shifts forward a little as long as it doesn’t go past right toe.",
        'exerDetails2': "If mobility allows, lightly tap left knee to the floor while keeping weight in right heel. Press into right heel to drive back up to starting position. Repeat on the other side.",
        'exerImgName': "lunges"
      }
    });
    return await modal.present();
  }

  async presentModal5() {
    
    const modal = await this.modalController.create({
      component: ModalPage,
      componentProps: {
        'exerName': "Burpees",
        'exerCount': "16",
        'exerDetails': "Start in a squat position with your knees bent, back straight, and your feet about shoulder-width apart. Lower your hands to the floor in front of you so they’re just inside your feet. With your weight on your hands, kick your feet back so you’re on your hands and toes, and in a pushup position.",
        'exerDetails2': "Keeping your body straight from head to heels, do one pushup. Remember not to let your back sag or to stick your butt in the air. Do a frog kick by jumping your feet back to their starting position. Stand and reach your arms over your head. Jump quickly into the air so you land back where you started. As soon as you land with knees bent, get into a squat position and do another repetition.",
        'exerImgName': "burpee"
      }
    });
    return await modal.present();
  }


}
