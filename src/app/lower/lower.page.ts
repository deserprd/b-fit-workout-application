import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ModalPage } from '../modal/modal.page';

@Component({
  selector: 'app-lower',
  templateUrl: './lower.page.html',
  styleUrls: ['./lower.page.scss'],
})
export class LowerPage implements OnInit {

  constructor(public modalController: ModalController) { }

  ngOnInit() {
  }

  async presentModal() {
    
    const modal = await this.modalController.create({
      component: ModalPage,
      componentProps: {
        'exerName': "Glute Bridge",
        'exerCount': "15",
        'exerDetails': "Lie on your back, arms down by your sides. Bend your knees and plant your feet flat on the floor. Pull in through your navel to brace your core muscles and then squeeze your glutes to press your hips up so your body forms a straight line — no arching — from knees to shoulders.",
        'exerDetails2': "Keep your head on the floor and eyes focused on the ceiling. Hold the position for a beat, and then lower and lift, and repeat.",
        'exerImgName': "glute"
      }
    });
    return await modal.present();
  }

  async presentModal2() {
    
    const modal = await this.modalController.create({
      component: ModalPage,
      componentProps: {
        'exerName': "Clamshell",
        'exerCount': "30",
        'exerDetails': "Lie on your right side with your feet and hips stacked, your knees bent 90 degrees, and your head resting on your right arm. Draw your knees in toward your body until your feet are in line with your butt. Place your left hand on your left hip to ensure it doesn’t tilt backward. This is your starting position.",
        'exerDetails2': "Keeping your abs engaged and your feet together, raise your left knee as high as you can while keeping your feet together and your right hip on the floor. Hold for 1 second, squeezing your glutes at the top of the move, before slowly lowering your left knee to the starting position. Perform equal reps on the other side.",
        'exerImgName': "clam"
      }
    });
    return await modal.present();
  }
  
  async presentModal3() {
    
    const modal = await this.modalController.create({
      component: ModalPage,
      componentProps: {
        'exerName': "Step-ups",
        'exerCount': "15",
        'exerDetails': "Stand tall holding a pair of dumbbells at arm’s length by your sides, and place your left foot on a bench so that your hip, knee, and ankle are all bent 90 degrees. Keeping your chest up and shoulders back, push your body up with your left leg until it’s straight (keep your right foot elevated).",
        'exerDetails2': "Pause, and then lower your body back to the starting position under control. Perform equal reps on both legs.",
        'exerImgName': "stepup"
      }
    });
    return await modal.present();
  }

  async presentModal4() {
    
    const modal = await this.modalController.create({
      component: ModalPage,
      componentProps: {
        'exerName': "Hip Thruster",
        'exerCount': "12",
        'exerDetails': "Place your upper back (lower scapula) against the center edge of the bench and place the weight bar across the hips. Squeeze the glutes and press the bar straight up until the hips are in line with the shoulders and the knees. ",
        'exerDetails2': " The bench should support the mid-scapula area. Keep the core tight and maintain a slight chin tuck with your focus down your body (a few inches above the bar). Slowly lower the bar down until the hips are just a few inches off the floor. Squeeze the glutes and lift again. Perform 10–12 repetitions.",
        'exerImgName': "hip"
      }
    });
    return await modal.present();
  }

  async presentModal5() {
    
    const modal = await this.modalController.create({
      component: ModalPage,
      componentProps: {
        'exerName': "Side Lunge",
        'exerCount': "12",
        'exerDetails': "Take a big step, lunging to the right side with the right foot. The right knee will bend deeply to accommodate the lunge and the hips will drop back. Keep the left leg straight, foot firmly grounded on the floor. The upper body remains tall and the chest stays open.",
        'exerDetails2': "Pushing off the right foot, lift the body and bring yourself back to the starting position with feet together. Repeat on the left side by stepping the left foot out to the side. Do 10–12 repetitions, alternating sides.",
        'exerImgName': "side"
      }
    });
    return await modal.present();
  }

}
