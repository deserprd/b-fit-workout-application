import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabapplePage } from './tabapple.page';

const routes: Routes = [
  {
    path: '',
    component: TabapplePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabapplePageRoutingModule {}
