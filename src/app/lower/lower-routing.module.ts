import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LowerPage } from './lower.page';

const routes: Routes = [
  {
    path: '',
    component: LowerPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LowerPageRoutingModule {}
