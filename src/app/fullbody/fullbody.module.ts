import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FullbodyPageRoutingModule } from './fullbody-routing.module';

import { FullbodyPage } from './fullbody.page';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FullbodyPageRoutingModule
  ],
  declarations: [FullbodyPage],
})
export class FullbodyPageModule {}
