import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabshangPage } from './tabshang.page';

const routes: Routes = [
  {
    path: '',
    component: TabshangPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabshangPageRoutingModule {}
