import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UpperPage } from './upper.page';

const routes: Routes = [
  {
    path: '',
    component: UpperPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UpperPageRoutingModule {}
