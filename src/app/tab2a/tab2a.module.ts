import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Tab2aPageRoutingModule } from './tab2a-routing.module';

import { Tab2aPage } from './tab2a.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Tab2aPageRoutingModule
  ],
  declarations: [Tab2aPage]
})
export class Tab2aPageModule {}
