import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FoodtabPageRoutingModule } from './foodtab-routing.module';

import { FoodtabPage } from './foodtab.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FoodtabPageRoutingModule
  ],
  declarations: [FoodtabPage]
})
export class FoodtabPageModule {}
