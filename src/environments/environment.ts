// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyBIbz9XXUmNsX_HxUKjIcOhX3z4t5_cg3w",
    authDomain: "finalprojectsummer-f27ce.firebaseapp.com",
    databaseURL: "https://finalprojectsummer-f27ce-default-rtdb.firebaseio.com",
    projectId: "finalprojectsummer-f27ce",
    storageBucket: "finalprojectsummer-f27ce.appspot.com",
    messagingSenderId: "640632591789",
    appId: "1:640632591789:web:7d2b74dea2142d9e41fd24",
    measurementId: "G-RBKKSDM0NH"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
