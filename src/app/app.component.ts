import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Home', url: '/home', icon: 'home', color: 'danger' },
    { title: 'Exercises', url: '/exercises', icon: 'barbell', color: 'secondary'},
    { title: 'Health & Food Tips', url: '/health', icon: 'pulse', color: 'success'},
    { title: 'About', url: '/about', icon: 'information-circle', color: 'success'},

  ];
  //public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];
  //insert app.html
  //<ion-item *ngFor="let label of labels" lines="none">
  //<ion-icon slot="start" ios="bookmark-outline" md="bookmark-sharp"></ion-icon>
  //<ion-label>{{ label }}</ion-label>
//</ion-item>
  constructor() {}
}
