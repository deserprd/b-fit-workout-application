import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TabapplePageRoutingModule } from './tabapple-routing.module';

import { TabapplePage } from './tabapple.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TabapplePageRoutingModule
  ],
  declarations: [TabapplePage]
})
export class TabapplePageModule {}
