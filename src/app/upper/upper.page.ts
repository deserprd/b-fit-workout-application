import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ModalPage } from '../modal/modal.page';

@Component({
  selector: 'app-upper',
  templateUrl: './upper.page.html',
  styleUrls: ['./upper.page.scss'],
})
export class UpperPage implements OnInit {

  constructor(public modalController: ModalController) { }

  ngOnInit() {
  }

  async presentModal() {
    
    const modal = await this.modalController.create({
      component: ModalPage,
      componentProps: {
        'exerName': "Diamond Press-up",
        'exerCount': "15",
        'exerDetails': "Get on all fours with your hands together under your chest. Position your index fingers and thumbs so they’re touching, forming a diamond shape, and extend your arms so that your body is elevated and forms a straight line from your head to your feet.",
        'exerDetails2': "Lower your chest towards your hands, ensuring you don’t flare your elbows out to the sides and keeping your back flat. Stop just before your chest touches the floor, then push back up to the starting position.",
        'exerImgName': "diamond"
      }
    });
    return await modal.present();
  }

  async presentModal2() {
    
    const modal = await this.modalController.create({
      component: ModalPage,
      componentProps: {
        'exerName': "Bench Press",
        'exerCount': "20",
        'exerDetails': "Lie flat on your back on a bench. Grip the bar with hands just wider than shoulder-width apart, so when you’re at the bottom of your move your hands are directly above your elbows. This allows for maximum force generation.",
        'exerDetails2': "Bring the bar slowly down to your chest as you breathe in. Push up as you breathe out, gripping the bar hard and watching a spot on the ceiling rather than the bar, so you can ensure it travels the same path every time.",
        'exerImgName': "bench"
      }
    });
    return await modal.present();
  }

  async presentModal3() {
    
    const modal = await this.modalController.create({
      component: ModalPage,
      componentProps: {
        'exerName': "Dumbbell Curls",
        'exerCount': "15",
        'exerDetails': "Stand or sit with a dumbbell in each hand, arms at your side, feet shoulder-width apart. Keep your elbows close to your torso and rotate the dumbbells so the palms of your hands are facing your body. This is your starting position.",
        'exerDetails2': "Take a deep breath and when you exhale, curl the weights upward while contracting your biceps. Pause at the top of the curl, then lower to the starting position. Repeat 10 to 15 times. Perform 2 to 3 sets.",
        'exerImgName': "dumbell"
      }
    });
    return await modal.present();
  }

  async presentModal4() {
    
    const modal = await this.modalController.create({
      component: ModalPage,
      componentProps: {
        'exerName': "Triceps Kickback",
        'exerCount': "15",
        'exerDetails': "Stand with a dumbbell in each hand, palms facing in toward each other. Keep your knees slightly bent. Keeping your spine straight, hinge forward at your waist so your torso is almost parallel to the floor. Engage your core. Keep your head in line with your spine, upper arms close to your body, and your forearms bent forward.",
        'exerDetails2': "As you exhale, hold your upper arms still while you straighten your elbows by pushing your forearms backward and engaging your triceps. Pause then inhale and return to the starting position. Repeat 10 to 15 times. Perform 2 to 3 sets.",
        'exerImgName': "tricep"
      }
    });
    return await modal.present();
  }

  async presentModal5() {
    
    const modal = await this.modalController.create({
      component: ModalPage,
      componentProps: {
        'exerName': "Triceps Dip",
        'exerCount': "15",
        'exerDetails': "Sit on a sturdy chair. Place your arms at your sides and your feet flat on the floor. Place your palms facing down beside your hips and grip the front of the seat. Move your body off the chair while gripping the seat. Knees should be slightly bent and your glutes should hover over the floor. Your arms should be fully extended, supporting your weight.",
        'exerDetails2': "Inhale and lower your body until your elbows form a 90-degree angle. Pause at the bottom, exhale, then push your body up to the starting position, squeezing your triceps at the top. Repeat 10 to 15 times. Perform 2 to 3 sets.",
        'exerImgName': "tricepdip"
      }
    });
    return await modal.present();
  }

}
