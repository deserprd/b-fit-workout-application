import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadingController, NavController } from '@ionic/angular';
import { IdeaService, Todo } from '../services/idea.service';


@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class DetailsPage implements OnInit {

  idea: Todo = {
    task: 'Test',
    createdAt: new Date().getTime(),
    priority: 2
  }
 
  ideadId = null;

  constructor(private ideaService: IdeaService, private route: ActivatedRoute, 
    private loadingController: LoadingController, private nav: NavController) { }
 
  ngOnInit() {
    this. ideadId = this.route.snapshot.params['id'];
    if(this.ideadId){
      this.loadIdea();
    }
  }

  async loadIdea() {
    const loading = await this.loadingController.create({
      message: 'Loading Todo..'
    });
    await loading.present();

    this.ideaService.getTodo(this.ideadId).subscribe(res => {
      loading.dismiss();
      this.idea = res;
    });
  }

  async saveIdea() {
    const loading = await this.loadingController.create({
      message: 'Saving Todo..'
    });
    await loading.present();

    if (this.ideadId) {
      this.ideaService.updateTodo(this.idea, this.ideadId).then(() => {
        loading.dismiss();
        this.nav.navigateBack('home');
      });
    } else {
      this.ideaService.addTodo(this.idea).then(() => {
        loading.dismiss();
        this.nav.navigateBack('home');
      });
    }
  }

}
