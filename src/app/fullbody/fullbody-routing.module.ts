import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FullbodyPage } from './fullbody.page';

const routes: Routes = [
  {
    path: '',
    component: FullbodyPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FullbodyPageRoutingModule {}
