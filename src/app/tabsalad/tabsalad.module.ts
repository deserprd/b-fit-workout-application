import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TabsaladPageRoutingModule } from './tabsalad-routing.module';

import { TabsaladPage } from './tabsalad.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TabsaladPageRoutingModule
  ],
  declarations: [TabsaladPage]
})
export class TabsaladPageModule {}
