import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  {
    path: 'folder/:id',
    loadChildren: () => import('./folder/folder.module').then( m => m.FolderPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'exercises',
    loadChildren: () => import('./exercises/exercises.module').then( m => m.ExercisesPageModule)
  },
  {
    path: 'health',
    loadChildren: () => import('./health/health.module').then( m => m.HealthPageModule)
  },
  {
    path: 'about',
    loadChildren: () => import('./about/about.module').then( m => m.AboutPageModule)
  },
  {
    path: 'details',
    loadChildren: () => import('./details/details.module').then( m => m.DetailsPageModule)
  },
  {
    path: 'details/:id',
    loadChildren: () => import('./details/details.module').then( m => m.DetailsPageModule)
  },
  {
    path: 'ccomment',
    loadChildren: () => import('./ccomment/ccomment.module').then( m => m.CcommentPageModule)
  },
  {
    path: 'ccomment/:id',
    loadChildren: () => import('./ccomment/ccomment.module').then( m => m.CcommentPageModule)
  },
  {
    path: 'csection',
    loadChildren: () => import('./csection/csection.module').then( m => m.CsectionPageModule)
  },
  {
    path: 'csection/:id',
    loadChildren: () => import('./csection/csection.module').then( m => m.CsectionPageModule)
  },
  {
    path: 'fullbody',
    loadChildren: () => import('./fullbody/fullbody.module').then( m => m.FullbodyPageModule)
  },
  {
    path: 'modal',
    loadChildren: () => import('./modal/modal.module').then( m => m.ModalPageModule)
  },
  {
    path: 'upper',
    loadChildren: () => import('./upper/upper.module').then( m => m.UpperPageModule)
  },
  {
    path: 'foodtab',
    loadChildren: () => import('./foodtab/foodtab.module').then( m => m.FoodtabPageModule)
  },
  {
    path: 'lower',
    loadChildren: () => import('./lower/lower.module').then( m => m.LowerPageModule)
  },
  {
    path: 'tab2',
    loadChildren: () => import('./tab2/tab2.module').then( m => m.Tab2PageModule)
  },
  {
    path: 'tabapple',
    loadChildren: () => import('./tabapple/tabapple.module').then( m => m.TabapplePageModule)
  },
  {
    path: 'tab2a',
    loadChildren: () => import('./tab2a/tab2a.module').then( m => m.Tab2aPageModule)
  },
  {
    path: 'tabsalad',
    loadChildren: () => import('./tabsalad/tabsalad.module').then( m => m.TabsaladPageModule)
  },
  {
    path: 'tabshang',
    loadChildren: () => import('./tabshang/tabshang.module').then( m => m.TabshangPageModule)
  },
  {
    path: 'tabtaco',
    loadChildren: () => import('./tabtaco/tabtaco.module').then( m => m.TabtacoPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
