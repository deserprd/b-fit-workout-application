import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.page.html',
  styleUrls: ['./modal.page.scss'],
})
export class ModalPage implements OnInit { 
  exerName: String;
  exerCount: String;
  exerDetails: String;
  exerDetails2: String;

  constructor(private modalController: ModalController, private navParams: NavParams) { }

  ngOnInit() {
    //this.passed_Id = this.navParams.get('custom_Id');
  }

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      'dismissed': true
    });
  }

}
