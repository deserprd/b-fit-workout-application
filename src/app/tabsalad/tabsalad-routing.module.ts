import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabsaladPage } from './tabsalad.page';

const routes: Routes = [
  {
    path: '',
    component: TabsaladPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsaladPageRoutingModule {}
